(function () {
    angular
        .module("sample")
        .controller("TimeoutCtrl", ["$timeout", "$log", TimeoutCtrl]);

    // Step 1 - Inject $timeout

    function TimeoutCtrl($timeout, $log) {
        // Step 2 - Expect $timeout
        var self = this;
        self.greeting = "...";
        self.greet = function () {
            // Step 3 Use setTimeout to display a greeting after 3 seconds

            setTimeout(function () {
                self.greeting = "Hello there! How are you doing today?";
                $log.log("Executed");
            }, 3000);

            // Step 3 Use $timeout to display a greeting after 3 seconds
            $timeout(function () {
                self.greeting = "Hello there! Updated by $timeout";
                $log.log("Executed");
            }, 3000);


        };

    }
})();
