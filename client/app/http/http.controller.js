(function () {
    angular
        .module("sample")
        .controller("HttpCtrl", ["$http", HttpCtrl]);
    // Step 1 - Inject $http above

    function HttpCtrl($http) {
        // Step 2 - Expect $http
        var self = this;

        self.products = [];

        // Step 3 - use http to GET /api/products
        $http
            .get("/api/products")
            .then(function (response) {
                // 2XX  1XX 3XX
                self.products = response.data;
            })
            .catch(function () {

                // 4XX 5XX
                console.log("Oops error occurred");
            });


        // Step 4 - add products to controller (self.products)

        // Step 5 - Open http.html and display products using ng-repeat

    }
})();