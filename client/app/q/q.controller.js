(function () {
    angular
        .module("sample")
        .controller("QCtrl", ["$http", QCtrl]);
    // Step 1: Inject $q and $http

    function QCtrl($http) {
        // Step 2: Expect $q and $http
        var self = this;

        self.products = [];

        $http
            .get("/api/products")
            .then(function (response) {
                self.products = response.data;
            })
            .catch(function () {
                alert("Error getting products")
            });


        var getProducts = function () {
            // Step 3: Implement this method
        };

        // Step 4: Use Promise
        getProducts();
    }
})();