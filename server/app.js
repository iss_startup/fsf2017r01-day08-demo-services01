var express = require("express");
var bodyParser = require('body-parser');

const NODE_PORT = process.env.NODE_PORT || 3000;

var app = express();

app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

app.use(express.static(__dirname + "/../client/"));

app.listen(NODE_PORT, function () {
    console.log("Server running at http://localhost:" + NODE_PORT);
});


app.get("/api/products", function (req, res) {
    res
        .json([{
            name: "Coca Cola",
            price: 10,
            discountedPrice: 8
        }, {
            name: "iPhone 7",
            price: 500,
            discountedPrice: 200
        }, {
            name: "iPad",
            price: 200,
            discountedPrice: 100
        }, {
            name: "LG 4K Smart LED TV",
            price: 5000,
            discountedPrice: 100
        }, {
            name: "Mac Book Pro",
            price: 3999,
            discountedPrice: 5
        }, {
            name: "Chicken Rice",
            price: 5,
            discountedPrice: 1
        }])
});